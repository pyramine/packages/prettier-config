/** @type {import('prettier').Options} */
module.exports = require('./dist/prettier-config');

// tailwind plugin is installed, but should not be used.
delete module.exports.tailwindConfig;
