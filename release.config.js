/** @type {import('@types/semantic-release').SemanticRelease.Options} */
module.exports = {
    plugins: [
        '@semantic-release/commit-analyzer',
        '@semantic-release/release-notes-generator',
        '@semantic-release/gitlab',
        '@semantic-release/npm',
    ],
    branches: [
        '+([0-9])?(.{+([0-9]),x}).x',
        'main',
        'next',
        'next-major',
        {
            name: 'alpha',
            prerelease: true,
        },
    ],
};
