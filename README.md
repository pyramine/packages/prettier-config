# Prettier Config

This package holds our pyramine.de Prettier config. It is used to get consistent formatting throughout our projects.

## Install
To install this package just run:

```shell
npm i -D -E @pyramine/prettier-config
```

> Note: this package only installs the prettier config. You need to install prettier separately.

# Configure
To use this configuration with your prettier installation you can simply put this into your `package.json`:

```json
{
  "prettier": "@pyramine/prettier-config"
}
```

Alternatively you can also use the `prettier.config.js` file:

```js
// just use default config:
module.exports = '@pyramine/prettier-config';
```

```js
// or if you want to extend the base config (not recomended):
const defaultConfig = require('@pyramine/prettier-config').default;

/** @type {import('prettier').Options} */
module.exports = {
    ...defaultConfig,
    singleQuote: false,
};
```
