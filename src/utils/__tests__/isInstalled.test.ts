import { isInstalled } from '../isInstalled';

it('should return true if the package is installed', () => {
    expect(isInstalled('prettier')).toBe(true);
});

it('should return false if the package is not installed', () => {
    expect(isInstalled('not-installed')).toBe(false);
});
