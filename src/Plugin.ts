import type { PrettierConfig } from './config';
import semver from 'semver';
import { isInstalled } from './utils/isInstalled';

export abstract class Plugin {
    protected abstract versionConstraint(): string;

    protected abstract packageName(): string;

    protected abstract modifyConfig(config: PrettierConfig): PrettierConfig;

    public apply(config: PrettierConfig): PrettierConfig {
        if (this.shouldApply()) {
            config.plugins.push(this.packageName());
            return this.modifyConfig(config);
        }

        return config;
    }

    protected shouldApply() {
        return isInstalled(this.packageName()) && this.checkVersionConstraint();
    }

    protected getPluginVersion() {
        // eslint-disable-next-line @typescript-eslint/no-require-imports
        return require(`${this.packageName()}/package.json`).version;
    }

    /**
     * return true if the version constraint is matched. Throws if constraint is not matched.
     * @protected
     */
    protected checkVersionConstraint(): true | never {
        const pluginVersion = this.getPluginVersion();

        if (!semver.satisfies(pluginVersion, this.versionConstraint())) {
            throw new Error(
                `The installed version of the '${this.packageName()}' (${pluginVersion}) is not supported. Only ${this.versionConstraint()} is supported.`,
            );
        }

        return true;
    }
}
