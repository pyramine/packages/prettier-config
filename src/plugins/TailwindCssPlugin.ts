import { Plugin } from '../Plugin';
import { PrettierConfig } from '../config';

export class TailwindCssPlugin extends Plugin {
    protected versionConstraint(): string {
        return '^0.1.13 || ^0.2.0 || ^0.3.0 || ^0.4.0 || ^0.5.0 || ^0.6.0';
    }

    protected packageName(): string {
        return 'prettier-plugin-tailwindcss';
    }

    protected modifyConfig(config: PrettierConfig): PrettierConfig {
        config.tailwindConfig = './tailwind.config.js';
        config.tailwindFunctions = [
            'classNames', // npm package: classNames
            'twMerge', // npm package: tailwind-merge
        ];

        return config;
    }
}
