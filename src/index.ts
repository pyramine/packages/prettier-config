import { config } from './config';

export type { PrettierConfig } from './config';

/**
 * Note: we can only export the config as `default` here.
 * Otherwise, Prettier will crash in the using projects.
 * Exporting types, however is fine, as they are gone during runtime.
 */
export default config;
