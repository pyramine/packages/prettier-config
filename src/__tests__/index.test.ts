/* eslint-disable @typescript-eslint/no-require-imports */
beforeEach(() => {
    jest.resetModules();
});

it('should match snapshot when prettier-plugin-tailwindcss is not installed', () => {
    jest.mock('../utils/isInstalled', () => ({
        isInstalled: () => false,
    }));

    expect(require('../index')).toMatchSnapshot();
});

it('should match snapshot when prettier-plugin-tailwindcss is installed', () => {
    jest.mock('../utils/isInstalled', () => ({
        isInstalled: () => true,
    }));

    expect(require('../index')).toMatchSnapshot();
});
