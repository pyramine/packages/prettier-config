import type { Plugin, Config } from 'prettier';
import { TailwindCssPlugin } from './plugins/TailwindCssPlugin';

/**
 * @public
 */
export interface PrettierConfig extends Config {
    /**
     * Provide ability to support new languages to prettier.
     * @see RequiredOptions.plugins
     */
    plugins: Array<string | Plugin>;

    /**
     * relative path to the tailwind configuration file.
     * Should only be set when the prettier-plugin-tailwindcss is installed.
     */
    tailwindConfig?: string;

    /**
     * You can sort additional attributes using the tailwindAttributes option,
     * which takes an array of attribute names
     */
    tailwindAttributes?: string[];

    /**
     * You can sort classes in function calls using the tailwindFunctions
     * option, which takes a list of function names:
     */
    tailwindFunctions?: string[];
}

const baseConfig: PrettierConfig = {
    singleQuote: true,
    bracketSameLine: true,
    tabWidth: 4,
    plugins: [],
    overrides: [
        {
            files: '**/*.{json,yml,yaml}',
            options: {
                tabWidth: 2,
            },
        },
        {
            files: '**/*.{yml,yaml}',
            options: {
                singleQuote: false,
                bracketSpacing: false,
            },
        },
        {
            files: 'src/routes/index.ts',
            options: {
                printWidth: 150,
            },
        },
    ],
};

const plugins = [TailwindCssPlugin] as const;

/**
 * @public
 */
export const config = plugins.reduce(
    (previousConfig, pluginClass) => new pluginClass().apply(previousConfig),
    baseConfig,
);
